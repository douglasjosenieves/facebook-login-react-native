import React, { Component } from "react";
import { View, Button, Text } from "react-native";
import { LoginButton, AccessToken } from "react-native-fbsdk";

export default class Login extends Component {
  constructor(props) {
    super(props);

    this.state = { user: [], uid: "" };
  }

  componentDidMount() {
    console.log(AccessToken);
    AccessToken.getCurrentAccessToken().then(data => {
      if (data) {
        const { accessToken } = data;
        console.log(accessToken);
        this._initUserFace(accessToken);
        ///  this.goToHomePage();
      }
    });
  }
  render() {
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <LoginButton
          publishPermissions={["public_profile"]}
          onLoginFinished={(error, result) => {
            if (error) {
              alert("login has error: " + result.error);
            } else if (result.isCancelled) {
              alert("login is cancelled.");
            } else {
              AccessToken.getCurrentAccessToken().then(data => {
                const { accessToken } = data;
                console.log(accessToken);
                this._initUserFace(accessToken);
                ///  this.goToHomePage();
              });
            }
          }}
          onLogoutFinished={this._outUserFace}
        />

        <Text>{JSON.stringify(this.state)}</Text>
      </View>
    );
  }

  _initUserFace = async token => {
    fetch("https://graph.facebook.com/v2.5/me?access_token=" + token)
      .then(response => response.json())
      .then(json => {
        this.setState({
          uid: json.id,
          user: json
        });
      })
      .catch(error => {
        console.log(error);
      });
  };

  _outUserFace = () => {
    alert("logout");
    this.setState({
      uid: "",
      user: ""
    });
  };
}
